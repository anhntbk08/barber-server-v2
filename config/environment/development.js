'use strict';

module.exports = {
  port: '9001',
  mysql: {
    username: "root",
    password: "123456",
    database: "Nodejs",
    host: "localhost",
    dialect: 'mysql',
    logging: false
  },
  cloudinary: {},
  userRoles: ['moderator', 'barber', 'user', 'admin'],
  google: {
    clientID: '204317021792-679ofhq6vr5j9k2cs3a1mrj6m6ici8tb.apps.googleusercontent.com',
    clientSecret: 'tMe7QGV8VDzsah61hz2UzFvP',
    callbackURL: 'http://localhost:9001/auth/google/callback'
  },
  facebook: {
    AppID: '578711698960155',
    AppSecret: '7ff32ba7e95e48defb61978b507ed80d',
    callbackURL: 'http://localhost:9001/auth/facebook/callback'
  },
  account_verify: {
    sender: 'bookstory286@gmail.com',
    password: 'nghiatt286',
    verify_link: 'http://localhost:9001/account/verify'
  }
};
