/**
 * Express configuration
 */

'use strict';

var express = require('express');
var favicon = require('serve-favicon');
var morgan = require('morgan');
var compression = require('compression');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var cookieParser = require('cookie-parser');
var errorHandler = require('errorhandler');
var path = require('path');
var config = require('./environment');
var passport = require('passport');
var redis      = require( 'redis' );
var session = require('express-session');
var redisStore = require( 'connect-redis' )( session );
var cors = require('cors');


// Create Redis client


var client = redis.createClient(config.redis.port, config.redis.host);
if( config.redis.username) {
  client.auth(config.redis.password, function (error) {
    if(error) {
      console.log(error);
    }
  })
}

module.exports = function(app) {
  var env = app.get('env');


  app.set('views', config.root + '/server/views');
  app.set('view engine', 'jade');
  app.use(compression());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());
  app.use(require('prerender-node').set('prerenderToken', '0fWEH1cujLVaIUytVYoG'));
  app.use(methodOverride());
  app.use(cookieParser());
  app.use(passport.initialize());
  app.use(cors());
  app.use( session(
    {
      secret: 'dliteme',
      store : new redisStore( {
        host  : 'localhost', // TODO: Production value to be entered
        port  : 6379,
        client: client
      } )
    }
  ) );
  if ('production' === env) {
   // app.use(favicon(path.join(config.root, 'public', 'favicon.ico')));
    app.use(express.static(path.join(config.root, 'public')));
    app.set('appPath', config.root + '/public');
    app.use(morgan('dev'));
  }

  if ('development' === env || 'test' === env) {
    app.use(require('connect-livereload')());
    app.use(express.static(path.join(config.root, '.tmp')));
    app.use(express.static(path.join(config.root, 'client')));
    app.set('appPath', 'client');
    app.use(morgan('dev'));
    app.use(errorHandler()); // Error handler - has to be last
  }

};

module.exports.redisClient = client;
