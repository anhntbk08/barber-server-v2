var storiesRouter = function(app) {
var book_service = require("./book.service");
  
  app.post("/book/info", function(req, res){
    book_service.getBookInfo(req.body.title, function(err, book){
      if (err){
        res.json(book);
      }
      else{
        res.json(403, {
          success: false,
          msg: "cant get book info"
        });
      }
    });
    
  });

  app.post("/book/content", function(req, res){
    book_service.getBookContent(parseInt(req.body.id), function(err, book){
      if (err){
        res.json(book);
      }
      else{
        res.json(403, {
          success: false,
          msg: "cant get book content"
        });
      }
    });
    
  });

  app.post("/book/user", function(req, res){
    book_service.listUserBook(parseInt(req.body.id), function(err, book){
      if (err){
        res.json(book);
      }
      else{
        res.json(403, {
          success: false,
          msg: "cant list user book"
        });
      }
    });
    
  });

}

module.exports = storiesRouter;
