var StripeModel = require("../services/stripe-model");
var FirebaseModel = require("../services/firebase-model");

var stripePath = '/api/stripe';

var appStripeRouter = function(app) {

  app.post(stripePath + '/cardCharge', function(req, httpRes) {
    try {
      var carInfo = req.body.chargeInfo;
      var description = req.body.chargeInfo.description || "";

      FirebaseModel
        .getHaircutPrice(req.body.bookingId)
        .then(function(res) {
          if (!res.data) return httpRes.status(400).send({
            error: true,
            msg: 'You trying to pay a non existed booking !!'
          });

          var price = res.data.price;

          if (!price) return httpRes.status(500).send({
            error: true,
            msg: 'Do not know the price of this booking, sorry about that !'
          });

          if (res.data.status !== 'booked') return httpRes.status(500).send({
            error: true,
            msg: 'Seem you has paid for this haircut already !'
          });

          StripeModel.charge(carInfo, parseFloat(price) * 100, "")
            .then(function(response) {
              FirebaseModel.finishHaircut(req.body.bookingId, req.body.userId);
              httpRes.send(response);
            }, function(error) {
              httpRes.status(500).send(error);
            });
        }, function(error) {
          console.log(error);
        })
    } catch (exception) {
      httpRes.status(500).send({
        error: true,
        msg: exception.toString()
      });
    }

  });

};

module.exports = appStripeRouter;