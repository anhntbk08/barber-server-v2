var StripeModel = require("../services/stripe-model");
var FirebaseModel = require("../services/firebase-model");
var auth = require("../auth/auth.service");
var bookingPath = '/api/booking';

var appBookingRouter = function(app) {

  /*
    Payment when booking
    each booking time, user must pay $1
   */
  
  app.post("/test", auth.hasRole('admin'), auth.hasPermission("STAFF_MANAGEMENT", "READ"), function(req, res){
       res.send('aaaaaaaaaa');
  });
  
  app.post(bookingPath + '/makeAppt',function(req, res) {

    var carInfo = req.body.chargeInfo;
    var amound = 100; // as cents
    var description = req.body.chargeInfo.description || "";

    StripeModel.charge(carInfo, amound, description)
      .then(function(response) {
        // switch firebase Status to booked
        FirebaseModel.finishBookingFlow(req.body.bookingId, req.body.userId);
        res.send(response);
      }, function(error) {
        res.status(500).send(error);
      });

  });

};

module.exports = appBookingRouter;
