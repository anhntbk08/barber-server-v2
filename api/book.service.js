var models = require('../models');

function getBookInfo(title, callback){
	models.books.findOne({
      where: {
        title: title
      }
    }).then(function(result) {
      // exist other account
      if (result) {
        return callback(true, result);
      }
      else{
      	return callback(false, null); 
      }

    });
}

function getBookContent(book_id, callback){
	models.sequelize
	.query('Select bc.index, s.image_link, s.text FROM book_contents as bc, stories as s ' +
	  'Where bc.book = ' + book_id + ' and bc.story = s.id', {
	    type: models.sequelize.QueryTypes.SELECT
	  })
	.then(function(data) {
	  if (!data.length){
	  	callback(false, null);
	  }
	  else{
	  	callback(true, data);
	  }
	});
}
function listUserBook(user_id, callback){
	models.sequelize
	.query('Select b.* FROM user_books as ub, books as b ' +
	  'Where ub.user = ' + user_id + ' and ub.book = b.id', {
	    type: models.sequelize.QueryTypes.SELECT
	  })
	.then(function(data) {
	  if (!data.length){
	  	callback(false, null);
	  }
	  else{
	  	callback(true, data);
	  }
	});
}
exports.getBookInfo = getBookInfo;
exports.getBookContent = getBookContent;
exports.listUserBook = listUserBook;