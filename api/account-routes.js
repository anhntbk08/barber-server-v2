var accountRouter = function(app) {
var auth = require("../auth/auth.service");
  /*
    Payment when booking
    each booking time, user must pay $1
   */
  
  app.post("/account/create", function(req, res){
    
    auth.createLocalUser({name: req.body.name,
        email: req.body.email,
        role: 'user',
        password: req.body.password
       
      }, function(err, user){
        if (err){
          res.json(401, {
          success: true,
          data: user,
          msg: 'create use success'
          });
        }
        else{
          res.json(401, {
          success: false,
          data: null,
          msg: 'cant create use success'
          });
        }
      });
  });

  app.get("/account/verify", function(req, res){
    active_code = req.param('id');
    auth.verifyEmail(active_code, function(err, msg){
      if (err){
          res.json(401, {
          success: true,
          data: null,
          msg: 'verify email successfully'
        });
      }
      else{
        res.json(401, {
          success: false,
          data: null,
          msg: 'cant active account'
        });
      }
    });
  });
};

module.exports = accountRouter;
