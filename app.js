var express = require('express');

var bodyParser       = require('body-parser');
var cookieParser     = require('cookie-parser');
var session          = require('express-session');
var app = express();

app.use( cookieParser()); 
app.use( bodyParser.json());
app.use( bodyParser.urlencoded({
		extended: true
	}));
	app.use( session({ 
		secret: 'cookie_secret',
		name:   'kaas',
		proxy:  true,
	    resave: true,
	    saveUninitialized: true
	}));



process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var config = require('./config/environment');
var server = require('http').createServer(app);
var models = require('./models');

require('./routes')(app);

models.sequelize.sync().then(function(db) {
  server.listen(config.port, function() {
    console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
  });
}).catch(function(error) {
  console.log('error occurred : ', error);
});

exports = module.exports = app;
