/**
 * Main application routes
 */

'use strict';
var bodyParser = require('body-parser')

module.exports = function(app) {

  app.use(bodyParser.json()); // support json encoded bodies
  app.use(bodyParser.urlencoded({
    extended: true
  }));

  app.set('port', (process.env.PORT || 8989));

  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

  app.use(function(req, res, done) {
    res.envelope = function(data, err) {
      var code = res.statusCode;

      // When there are data in the returned object
      res.json({
        data: data || null,
        meta: (err) ? {
          messages: [err.message || err.msg]
        } : {},
        status: (code >= 200 ? 'success' : 'error'),
        statusCode: code
      })

    };

    res.error = function(statusCode, message) {

      var statusMsg;
      switch (statusCode) {

        case 305:
          statusMsg = 'Postcode_Unavailable';
          break;
        case 400:
          statusMsg = 'Bad Request';
          break;
        case 404:
          statusMsg = 'Unrecognised request';
          break;
        case 500:
          statusMsg = 'Server Error';
          break;
        case 401:
          statusMsg = 'Unauthorized';
          break;

          return statusCode;

      }

      res.json({
        status: statusMsg,
        statusCode: statusCode,
        msg: message
      })
    };
    done();
  });

  require("./api/stripe-routes.js")(app);
  require("./api/booking-routes.js")(app);

  app.use('/auth', require('./auth'));

};
