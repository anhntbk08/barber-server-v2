var passport = require('passport');
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

exports.setup = function(User, config, app) {
 
  
  app.use( passport.initialize());
  app.use( passport.session());  
  
  passport.serializeUser(function(user, done) {
   console.log("serializeUser");
   //console.log(user);
      done(null, user);
  });

passport.deserializeUser(function(obj, done) {
   console.log("deserializeUser");
   //console.log(obj);
      done(null, obj);
   });


  passport.use(new GoogleStrategy({
      clientID: config.google.clientID,
      clientSecret: config.google.clientSecret,
      callbackURL: config.google.callbackURL
    },
    function(accessToken, refreshToken, profile, done) {
     
      User.findOne({
        'google_id': profile.id,
        'provider': 'google'
      }).then(function(err, user) {
        if (!user) {
          console.log("Create new user");
          User.createUser({
            name: profile.displayName,
            email: profile.emails[0].value,
            role: 'user',
            username: profile.username,
            provider: 'google',
            google: profile._json,
            google_id: profile.id
          }, function(c, user) {
            if (c == -1 || c == -2) return done(null, false);
            console.log("passport done call1");
            done(null, user);
          });
        } else {
          console.log("passport done call2");
          return done(null, user);
        }
      });
    }
  ));
};
