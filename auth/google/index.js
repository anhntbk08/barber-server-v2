'use strict';

var express = require('express');
var passport = require('passport');
var auth = require('../auth.service');

var router = express.Router();

router
  .get('/', passport.authenticate('google', {
    failureRedirect: '/signup',
    successRedirect: '/success',
    scope: [
      'https://www.googleapis.com/auth/userinfo.profile',
      'https://www.googleapis.com/auth/userinfo.email',
      'https://www.googleapis.com/auth/calendar'
    ],
    session: false
  }))

.get('/callback', passport.authenticate('google', {
  failureRedirect: '/signup',

}), auth.setTokenCookie)

.get("/success", function(req, res){

       res.send("login successfully");
  })

.post("/lon", auth.hasRole('admin'), auth.hasPermission("STAFF_MANAGEMENT", "READ"), function(req, res){
       
       res.send("aaaaaa");
  });

module.exports = router;
