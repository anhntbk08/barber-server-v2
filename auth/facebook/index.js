'use strict';

var express = require('express');
var passport = require('passport');
var auth = require('../auth.service');

var router = express.Router();

router
  .get('/', passport.authenticate('facebook', {
    scope: ['email', 'user_about_me'],
    failureRedirect: '/signup',
  }))

  .get('/callback', passport.authenticate('facebook', {
    failureRedirect: '/signup',
    
  }), auth.setTokenCookie)

.get("/success", function(req, res){

       res.send("login successfully");
  })



module.exports = router;
