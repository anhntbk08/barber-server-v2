var passport = require('passport');
var FacebookStrategy = require('passport-facebook').Strategy;
console.log("Facebook Start Login");
exports.setup = function(User, config, app) {

  
   app.use( passport.initialize());
   app.use( passport.session());  
  
   passport.serializeUser(function(user, done) {
    console.log("serializeUser");
   //console.log(user);
      done(null, user);
   });

   passport.deserializeUser(function(obj, done) {
   console.log("deserializeUser");
   //console.log(obj);
      done(null, obj);
   });

  passport.use(new FacebookStrategy({
      clientID: config.facebook.AppID, 
      clientSecret: config.facebook.AppSecret, 
      callbackURL: config.facebook.callbackURL, 
      enableProof: true
    },
    function(accessToken, refreshToken, profile, done) {
      User.findOne({
        'facebook_id': profile.id,
        'provider': 'facebook'
      }).then(function(err, user) {

        if (!user) {
          var email = '';
          if (profile["emails"] != undefined)
          {
             email = profile.emails[0].value;
          }
          User.createUser({
            name: profile.displayName,
            email: email,
            role_id: 1,
            username: profile.username,
            provider: 'facebook',
            facebook: profile._json,
            facebook_id: profile.id
          }, function(c, user) {
            if (c == -1 || c == -2) return done(null, false);
            done(null, user);
          });
        } else {
          return done(null, user);
        }
      });
    }
  ));
};
