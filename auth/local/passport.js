var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var models = require('../../models');
var winston = require('winston');
var logger = new winston.Logger();

var USER_STATUS = {
  ACTIVE: "1",
  DELIVERING: "2",
  INACTIVE: "3"
};


exports.setup = function(User, config, app) {
  passport.use(new LocalStrategy({
      usernameField: 'email',
      passwordField: 'password' // this is the virtual field on the model
    },
    function(email, password, done) {
      User.findOne({
        where: {
          email: email.toLowerCase()
        }
      }).then(function(user) {
        if (!user) {
          logger.info('This email is not registered');
          return done(null, false, {
            message: 'This email is not registered.',
            success: false
          });
        }

        if (user.status == USER_STATUS.INACTIVE) {
          return done(null, false, {
            message: 'This email is not verified.',
            success: false
          });
        }

        if (!user.authenticate(password)) {
          return done(null, false, {
            message: 'This password is not correct.',
            success: false
          });
        }
        var returnedUser = {
          id: user.id,
          email: user.email,
          name: user.name,
          customer: user.customers,
          phoneno: user.phoneno,
          status: user.status,
          // createdAt: user.createdAt,
          updatedAt: user.updatedAt
        };
        return done(null, returnedUser);
      });
    }
  ));
};