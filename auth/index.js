'use strict';

var express = require('express');
var passport = require('passport');
var config = require('../config/environment');
var models = require('../models');

// Passport Configuration
module.exports = function(app)
{
	require('./local/passport').setup(models.users, config, app);
	require('./facebook/passport').setup(models.users, config, app);
	require('./google/passport').setup(models.users, config, app);
	//require('./twitter/passport').setup(models.User, config);

	var router = express.Router();

	router.use('/local', require('./local'));
	router.use('/facebook', require('./facebook'));
	//router.use('/twitter', require('./twitter'));
	router.use('/google', require('./google'));
        return router;
};

//module.exports = router;
