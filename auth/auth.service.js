'use strict';

var passport = require('passport');
var config = require('../config/environment');
var jwt = require('jsonwebtoken');
var expressJwt = require('express-jwt');
var compose = require('composable-middleware');
var models = require('../models');
var _ = require('lodash');
var sequelize = require('sequelize');
var validateJwt = expressJwt({
  secret: config.secrets.session
});
var winston = require('winston');
var logger = new winston.Logger();
var nodemailer = require('nodemailer');

var USER_STATUS = {
  ACTIVE: 1,
  DELIVERING: 2,
  INACTIVE: 3
};
/**
 * Attaches the user object to the request if authenticated
 * Otherwise returns 403
 */

function generateActiveCode(email){
  return Math.floor((Math.random() * 100) + 54);
}

function sendVerifyEmail(email, link){
  console.log("verify email");
  
  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: config.account_verify.sender,
        pass: config.account_verify.password
    }
  });
  transporter.sendMail({
      from: config.account_verify.sender,
      to: email,
      subject: 'verify email',
      text: 'Active:' + link
  });

}
function verifyEmail(active_code, callback){
   models.users.update({status: USER_STATUS.ACTIVE}, {
    where: {
      active_code: active_code
    }
  }).then(function(result) {
    if (!result) callback(false, 'can not verify user');
    callback(true, result);
  })
}
function createLocalUser(userInfo, callback){
  var active_code = generateActiveCode(userInfo.email);
  userInfo.provider = 'local';
  userInfo.status = USER_STATUS.INACTIVE;
  userInfo.active_code = active_code;
  models.users.findOne({
      where: {
        email: userInfo.email
      }
    }).then(function(result) {
      // exist other account
      if (result) {
        return callback(false, result);
      }

      models.users.create(userInfo)
        .then(function(user) {
          if (!user) return callback(false,'Unknown issue - Can\'t create user ');
          
          var active_link = config.account_verify.verify_link + '?id='+active_code;
          sendVerifyEmail(userInfo.email, active_link);
          callback(true, user);
        })
        .catch(function(exception) {
          return callback(false, null);
        });

    });
}


function setUserRole(userName, roleName, permission, callback)
{
   var Roles = models.roles;
   var Permission = models.permissions;
   Roles.createRole({name:'roleName'}, function(result, msg) {
      var role_id = -1;
      var permisson_id = -1;

      if (result){
          console.log("insert success");
          console.log(msg.id);
      }
      else {
          
      }
      if (role_id){
          Permission.createPermission(permission, function(result, msg) {
              if (result){
                  permisson_id = msg.id;
              }
              else {
			
              }
              if (permisson_id){
                  
              }
          }); 
      }
   });
};

function isAuthenticated() {
  return compose()
    // Validate jwt
    .use(function(req, res, next) {

      var token = req.body.access_token || req.headers.authorization || req.query.access_token || req.headers['x-auth-token'];
      console.log("isAuthenticated");
      console.log(token);
      if (req.headers && req.headers.hasOwnProperty('x-auth-token')) {
        req.headers.authorization = 'Bearer ' + token;
      }
      if (token){
        jwt.verify(token, config.secrets.session, function(err, decoded) {      
          if (err) {
            return res.json({ success: false, message: 'Failed to authenticate token.' });    
          } else {
        // if everything is good, save to request for use in other routes
            console.log(decoded);
            req.user = decoded;   
            next();
          }
        });
      }
      //validateJwt(req, res, next);
      //next();
    })
    // Attach user to request

  .use(function(req, res, next) {
    //console.log(req.user);
    models.users.findOne({
      where: {
        id: req.user.id,
        status: {
          $in: [USER_STATUS.ACTIVE, USER_STATUS.DELIVERING]
        }
      }
    }).then(function(user) {
      if (!user) return res.json(401, {
        success: false,
        data: null,
        msg: 'Exception occurred while fetching user. Unauthorized!'
      });

      req.user = user;
      next();
    }).catch(function(exception) {
      return res.json(401, {
        success: false,
        data: null,
        msg: 'Exception occurred while fetching user ' + exception
      });
    });
  });
}

/**
 * Deprecated - no longer using
 * Checks if the user role meets the minimum requirements of the route
 */
function hasRole(roleRequired) {
  if (!roleRequired) throw new Error('Required role needs to be set');

  return compose()
    .use(isAuthenticated())
    .use(function meetsRequirements(req, res, next) {
      // if (config.userRoles.indexOf(req.user.role_id) >= config.userRoles.indexOf(roleRequired)) {
      //   next();
      // }
      // else {
      //   res.send(403);
      // }
       
       
       models.role_permission.findOne({
          where: {
              id: req.user.role_id
          }
       }).then(function(result) {
              if (result) {    
                models.roles.findOne({
			  where: {
			    id: result.role
			  } 
			}).then(function(result) {
			  if (result) {
          if (result.name == roleRequired){
             return next();
          }
			  }
			  
			  return res.json(401, {
				success: false,
				data: null,
				msg: '1Exception occurred while fetching user. Unauthorized!'
			      });
			  
			});   
              }
              else{
                 return res.json(401, {
		 success: false,
		 data: null,
		 msg: '2Exception occurred while fetching user. Unauthorized!'
	         });
                       
              }
          });
        
    });
}

/**
 * Checks if the user role meets the minimum requirements of the route
 */
function hasPermission(permissionRequired, action) {

  if (!permissionRequired || !action) throw new Error('Required permission/action needs to be set');

  return compose()
    .use(isAuthenticated())
    .use(function meetsRequirements(req, res, next) {
     
      // get permissions with current user role
      if (!req.user.role_id)
        return res.json(403, {
          success: false,
          msg: "User doesn't have permission to view this data"
        });

      models.sequelize
        .query('Select rp.value, p.alias, p.name from permissions as p, roles as r, role_permissions as rp ' +
          'Where rp.id = ' + req.user.role_id + ' and rp.role = r.id and rp.permission = p.id', {
            type: models.sequelize.QueryTypes.SELECT
          })
        .then(function(data) {
          if (!data.length)
            return res.json(403, {
              success: false,
              msg: "User doesn't have permission to view this data"
            });

          var permissions = data;
          console.log(data);
          var userPermission = _.find(permissions, function(permission) {
            return permission.alias == permissionRequired;
          });
          if (userPermission) {
            req.user.permissions = permissions;

            //FIND OUT PERMISSION VALUE
            if (userPermission['READ'] = userPermission.value >> 3 >= 1)
              userPermission.value -= 8;

            if (userPermission['UPDATE'] = userPermission.value >> 2 >= 1)
              userPermission.value -= 4;

            if (userPermission['CREATE'] = userPermission.value >> 1 >= 1)
              userPermission.value -= 2;

            if (userPermission['DELETE'] = userPermission.value >> 0 >= 1)
              userPermission.value -= 1;

            if (userPermission[action])
              next();
            else
              res.send(403);
          } else {
            res.json(403, {
              success: false,
              msg: "No permission to access route"
            });
          }
        });

    });
}

/**
 * Returns a jwt token signed by the app secret
 */
function signToken(id, role_id) {
  return jwt.sign({
    id: id,
    role_id: role_id
  }, config.secrets.session, {
    expiresInMinutes: 60 * 5
  });
}

/**
 * Set token cookie directly for oAuth strategies
 */
function setTokenCookie(req, res) {
  
  if (!req.user ) return res.status(404).json({
    message: 'Something went wrong, please try again.'
  });
  var token = signToken(req.user.id, req.user.role_id);
  console.log(token);
  //res.cookie('access_token', JSON.stringify(token));
  //console.log('inside set Token cookies ...');
  //res.redirect('/');
  res.json({
          success: true,
          message: 'Enjoy your token!',
          token: token
        });
  
}



exports.isAuthenticated = isAuthenticated;
exports.hasRole = hasRole;
exports.hasPermission = hasPermission;
exports.signToken = signToken;
exports.setTokenCookie = setTokenCookie;
exports.createLocalUser = createLocalUser;
exports.verifyEmail = verifyEmail;