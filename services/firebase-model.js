function FirebaseModel() {};

var StripeModel = require("./stripe-model");
var q = require("q");
var moment = require("moment-timezone");
var Firebase = require("firebase");
var https = require('https');


var fb = new Firebase("https://clypr-7.firebaseio.com/");
moment.tz.setDefault("America/New_York");

/*
 * Performs Firebase transaction() operation
 */
FirebaseModel.finishBookingFlow = function(bookingId, userId) {
  var bookingRef = fb.child('bookings/' + bookingId);
  bookingRef.update({
    status: 'booked'
  }, function(error) {
    console.log(error);
    var userBookingRef = fb.child('user_bookings/' + userId);
    var userBookingUpdateData = {};
    userBookingUpdateData[bookingId] = 'booked';
    userBookingRef.update(userBookingUpdateData, function(error) {
      // deferred.resolve(error);
    })
  })
};


/*
  Get cost from Firebase
 */
FirebaseModel.getHaircutPrice = function(bookingId) {
  var deferred = q.defer();

  var bookingRef = fb.child('bookings/' + bookingId);
  bookingRef.once("value", function(snapshot) {
    return deferred.resolve({success: true, data: snapshot.val()});
  }, function(error) {
    return deferred.reject(error);
  });

  return deferred.promise;
}


/*
  Finish haircut
 */
FirebaseModel.finishHaircut = function(bookingId, userId) {
  var bookingRef = fb.child('bookings/' + bookingId);
  bookingRef.update({
    status: 'done'
  }, function(error) {
    var userBookingRef = fb.child('user_bookings/' + userId);
    var userBookingUpdateData = {};
    userBookingUpdateData[bookingId] = 'done';
    userBookingRef.update(userBookingUpdateData, function(error) {
      // deferred.resolve(error);
    })
  })
}
module.exports = FirebaseModel;