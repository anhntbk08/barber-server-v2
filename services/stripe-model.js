var StripeModel = {};

var q = require("q");
var stripe = require("stripe")(
  "sk_test_rJLQl8m7PO7J3a8xuJfNwaYi"
);

stripe.setApiKey('sk_test_rJLQl8m7PO7J3a8xuJfNwaYi');

var https = require('https');

var options = {
  hostname: 'api.stripe.com',
  path: '/v1/charges',
  method: 'POST',
  headers: {
    'Authorization': 'Bearer sk_test_rJLQl8m7PO7J3a8xuJfNwaYi'
  }
};

/*
 * Function that computed reservation fee amount
 */
StripeModel.computeReserveFee = function(estCost) {
  var percent = 6;
  var transFee = 60; // 60 cents fee on top of 6% of estimated cost
  var tmpFee = estCost * percent / 100 + transFee;
  var resFee = Math.max(500, tmpFee)
  return resFee;
};

/*
 * Sample Function that performs a Stripe
 * successful test charge using hardcoded
 * card data.
 *
 */
StripeModel.charge = function(cardInfo, amount, description) {
  var deferred = q.defer();

  stripe.charges.create({
    amount: amount,
    currency: "usd",
    source: cardInfo,
    description: description
  }, function(err, charge) {
    // asynchronously called
    if (err) {
      return deferred.reject(err);
    } else {
      return deferred.resolve(charge);
    }
  });

  return deferred.promise;
};


module.exports = StripeModel;