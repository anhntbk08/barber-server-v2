
module.exports = function(sequelize, DataTypes) {
  var role_permission = sequelize.define('role_permission', {
    id: {
      type: DataTypes.INTEGER(11),
      autoIncrement: true,
      primaryKey: true
    },
    role: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
    },
    permission: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
    },
    value: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
    },
   
  }, {
    classMethods: {
      
      createRolePermission: function(rolePermisstionInfo, callback) {
        this.findAll({
          where: {
            role: rolePermisstionInfo.role,
            permission: rolePermisstionInfo.permission
          }
        }).then(function(result) {
          // exist other account
          if (result.length) {
            return callback({
              success: false,
              msg: 'Permisstion exist'
            })
          }

          // no exist - can create new Role
          this.create(rolePermisstionInfo)
            .then(function(role_permisstion) {
              if (!role_permisstion) callback({
                sucess: false,
                msg: 'Unknown issue - Can\'t create new Role '
              });
              callback({
                success: true,
                role_permisstion: role_permisstion
              });
            })
            .catch(function(exception) {
              return callback({
                success: false,
                msg: exception.toString()
              });
            });

        });
      },
	
      findByRPId: function(role_permission_id, callback) {
      
       this.findOne({
          where: {
            id: role_permission_id
          }
        }).then(function(result) {
          if (result) {
            return callback(true, result);
          }
          else{
             return callback(false, "cant find role-permisstion");
          }
        });
      },
      

      updatePermission: function(permissionInfo, callback) {
        this.update(permissionInfo, {
          where: {
            id: permissionInfo.id
          }
        }).then(function(result) {
          if (!result) callback({
            sucess: false,
            msg: 'Unknow issue - Can\'t update Permission'
          });
          callback({
            success: true
          });
        }).catch(function(exception) {
          return callback({
            success: false,
            msg: exception.toString()
          });
        });
      },

    },

  });

  return role_permission;
};
