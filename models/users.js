var crypto = require('crypto');

module.exports = function(sequelize, DataTypes) {
  var users = sequelize.define('users', {
    id: {
      type: DataTypes.INTEGER(11),
      autoIncrement: true,
      primaryKey: true
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isValidateEmail: function(email) {
          return true;
        }
      }
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    password: {
      type: DataTypes.VIRTUAL,
      set: function(val) {
        this._password = val;
        this.hashedpassword = this.encryptPassword(val);

        // this.setDataValue('password', val); // Remember to set the data value, otherwise it won't be validated
        // this.setDataValue('hashedPassword', this.salt + val);
      },
      get: function() {
        return this._password;
      }
    },
    hashedpassword: {
      type: DataTypes.STRING,
      allowNull: true
    },
    phoneno: {
      type: DataTypes.STRING,
      allowNull: true
    },
    status: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 1
    },
    role_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    provider: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: 'local'
    },
    type: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: 'customer'
    },
    google_id: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    facebook_id: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    active_code: {
      type: DataTypes.STRING,
      allowNull: true
    },
    refresh_token: {
      type: DataTypes.STRING,
      allowNull: true
    }
   
    // token: { TODO: We havent added this into DB yet
    //   type: DataTypes.STRING
    // },
    // token_expired: {
    //   type: DataTypes.DATE
    // }
  }, {
    classMethods: {
      associate: function(models) {
        users.belongsTo(models.roles, {
          foreignKey: 'role_id'
        });
      },
      /**
       * Creates a new user
       * @param {email}
       * @param {phoneno}
       * @param {password}
       * @param {name}
       * @param {role}
       * @param {type}
       * @result {Object} {sucess: true/false, id: 'in success case'}
       */
      createUser: function(userInfo, callback) {

        /*
          Check if exist user with email or phoneno
        */
        this.findOne({
          where: {
            email: userInfo.email
          }
        }).then(function(result) {
          // exist other account
          if (result) {
            return callback(0, result);
          }

          this.create(userInfo)
            .then(function(user) {
              if (!user) callback(-1,'Unknown issue - Can\'t create user ');
              callback(1, user);
            })
            .catch(function(exception) {
              return callback(-2, exception);
            });

        });
      },
 
      findUserByID: function(user_id, callback) {
       this.findOne({
          where: {
            id: user_id
          }
        }).then(function(result) {
          if (result) {
            return callback(true, result);
          }
          else {
             return callback(false, "Cant not find user");
          }
        });
      },


      updateUser: function(userInfo, callback) {
        this.update(userInfo, {
          where: {
            id: userInfo.id
          }
        }).then(function(result) {
          if (!result) callback({
            sucess: false,
            msg: 'Unknow issue - Can\'t create user '
          });
          callback({
            success: true
          });
        }).catch(function(exception) {
          return callback({
            success: false,
            msg: exception.toString()
          });
        });
      },

    },
    instanceMethods: {
      /**
       * Authenticate - check if the passwords are the same
       *
       * @param {String} plainText
       * @return {Boolean}
       * @api public
       */
      authenticate: function(plainText) {
        return this.encryptPassword(plainText) === this.hashedpassword;
      },

      /**
       * Encrypt password
       *
       * @param {String} password
       * @return {String}
       * @api public
       */
      encryptPassword: function(password) {
        if (!password) return '';
        var md5 = crypto.createHash('md5');
        md5.update(password);
        return md5.digest('hex');
      }
    }
  });
  
  console.log('testing ...');
  return users;
};
