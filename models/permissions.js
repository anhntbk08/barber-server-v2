//module.exports = function(sequelize, DataTypes) {
//  return sequelize.define('permissions', {
//    id: {
//      type: DataTypes.INTEGER(11),
//      autoIncrement: true,
//      primaryKey: true
//    },
//    name: {
//      type: DataTypes.STRING,
//      allowNull: false,
//    },
//    alias: {
//      type: DataTypes.STRING,
//      allowNull: false,
//    }
//  });
//};


module.exports = function(sequelize, DataTypes) {
  var permissions = sequelize.define('permissions', {
    id: {
      type: DataTypes.INTEGER(11),
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    alias: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
    },
   
  }, {
    classMethods: {
      
      createPermission: function(permisstionInfo, callback) {

        this.findAll({
          where: {
            name: permisstionInfo.name, 
            alias:permisstionInfo.alias
          }
        }).then(function(result) {
          // exist other account
          if (result.length) {
            return callback({
              success: false,
              msg: 'Permisstion exist'
            })
          }

          // no exist - can create new Role
          this.create(permisstionInfo)
            .then(function(permisstion) {
              if (!permisstion) callback({
                sucess: false,
                msg: 'Unknown issue - Can\'t create new Role '
              });
              callback({
                success: true,
                permisstion: permisstion
              });
            })
            .catch(function(exception) {
              return callback({
                success: false,
                msg: exception.toString()
              });
            });

        });
      },

      updatePermission: function(permissionInfo, callback) {
        this.update(permissionInfo, {
          where: {
            id: permissionInfo.id
          }
        }).then(function(result) {
          if (!result) callback({
            sucess: false,
            msg: 'Unknow issue - Can\'t update Permission'
          });
          callback({
            success: true
          });
        }).catch(function(exception) {
          return callback({
            success: false,
            msg: exception.toString()
          });
        });
      },

    },

  });

  return permissions;
};
