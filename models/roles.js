module.exports = function(sequelize, DataTypes) {
  var roles = sequelize.define('roles', {
    id: {
      type: DataTypes.INTEGER(11),
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
    },
   
  }, {
    classMethods: {
      
      createRole: function(roleInfo, callback) {

        this.findOne({
          where: {
            name: roleInfo.name
          }
        }).then(function(result) {
          // exist other account
          if (result) {
            console.log("nhay vao day roi");
            return callback(
              false,
              result
            );
          }
	  console.log("khong tim thay rule name");
          // no exist - can create new Role
          this.create(roleInfo)
            .then(function(role) {
              if (!role) callback({
                sucess: false,
                msg: 'Unknown issue - Can\'t create new Role '
              });
              callback(true, role);
            })
            .catch(function(exception) {
              return callback({
                success: false,
                isExsit: false,
                msg: exception.toString()
              });
            });

        });
      },
      
       findRoleByID: function(role_id, callback) {
       this.findOne({
          where: {
            id: role_id
          }
        }).then(function(result) {
          if (result) {
            return callback(true, result);
          }
          else{
             return callback(false, "cant find role");
          }
        });
      },

      updateRole: function(roleInfo, callback) {
        this.update(roleInfo, {
          where: {
            id: roleInfo.id
          }
        }).then(function(result) {
          if (!result) callback({
            sucess: false,
            msg: 'Unknow issue - Can\'t update Role'
          });
          callback({
            success: true
          });
        }).catch(function(exception) {
          return callback({
            success: false,
            msg: exception.toString()
          });
        });
      },

    }
  },
   {
     timestamps: false
  });

  return roles;
};
