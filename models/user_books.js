module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user_books', {
    id: {
      type: DataTypes.INTEGER(11),
      autoIncrement: true,
      primaryKey: true
    },
    user: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
    },
    book: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
    }
  });
};