module.exports = function(sequelize, DataTypes) {
  return sequelize.define('book_contents', {
    id: {
      type: DataTypes.INTEGER(11),
      autoIncrement: true,
      primaryKey: true
    },
    book: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
    },
    story: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
    },
    index: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
    }
  });
};